# Chapter 1 - A Tutorial Introduction

Escape Sequences:
* ```\n``` - new line
* ```\t``` - tab
* ```\b``` - backspace
* ```\"``` - double quote
* ```\\``` - backslash

Comments can be written as:
```c 
// for single line
/* for 
multi
line */
```
##### 1.2 Variable and Arithmetic Expressions
[Fahrenheit / Celsius Table Program](ch-1-proj/1-2.c)

* In C, all variables must be declared before they are used. 
* A *declaration* announces the properties of variables; it consists of a type name nd a list of variables.
Example: 
```c 
int fahr, celsius; 
int lower, upper, step;
```

* The type ``int`` means that the variable listed are inegers. 
* ``float`` means floating point numbers (decimals).
* ``short`` - short integer
* ``char`` - character, single byte
* ``long`` - long integer
* ``double`` - double-precision floating point

* ``array`` - 
* ``structures`` - 
* ``unions`` - 

In our example program, computation in the temperature conversion program begins with the assignment statements
```c
lower = 0;
upper = 300;
step = 20;
fahr = lower;
```
which set the varibale to their initial values. 

Individual statements are terminated by a semicolon ``;``.

``%d`` - argument integer type
* each argument refrenced by the ``%`` sign needs to match up with the corrispondeing argument and type. 

[Fahrenheit / Celsius Table Program - Floating Point Version](ch-1-proj/1-2v2-float.c)
* If an arithmetic operator has integer operands, and interger operation is performed. 
* If an arithmetic operation has one floating=point operand adn one integer operand, the ineter will be converted to floating point before the oeration is done. 

* ``%d`` - print as decimal point
* ``%6d`` - print as decimal integer, at least 6 characters wide
* ``%f`` - print as floating point
* ``%6f`` - print as floating point, at least 6 characters wide
* ``%.2f`` - print as floating point, 2 characters after the decimal point
* ``6.2f`` - print as floating point, at least 6 characters wide adn 2 after the decimal point

* ``%o`` - for octal
* ``%x`` - for hexidecimal
* ``%s`` - for character string
* ``%%`` - for percent itself

[Exercise 1-3 - Modify the temperature conversion program to print a heading above the table]()
[Exercise 1-4 - Write a program to print the correspoinding Celsius to Fahrenheit table]()

[Table of Contents](README.md)
