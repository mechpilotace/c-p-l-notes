# The C Programming Language, 2nd Edition
This repo is for notes as I work through this book. 

## Table of Contents
### [Chapter 1 - A Tutorial Introduction](ch-1.md)
### [Chapter 2 - Types, Operators and Expressions](ch-2.md)
### [Chapter 3 - Control Flow](ch-3.md)
### [Chapter 4 - Functions and Program Struture](ch-4.md)
### [Chapter 5 - Pointers and Arrays](ch-5.md)
### [Chapter 6 - Structures](ch-6.md)
### [Chapter 7 - Inputs and Outputs](ch-7.md)
### [Chapter 8 - The Unix System Interface](ch-8.md)
