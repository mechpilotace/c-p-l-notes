# Chapter 3 - Control Flow

The control-flow statements of a language specify the order in which computations are performed.

##### 3.1 Statement and Blocks

* In C, the semicolon is the statemnet terminator. 
* Braces ```{ and }``` are used to group declarations and statements together into a *compound statement*. 

###### 3.2 If-Else
The if-else statement is used to express decisions. 
* the *else* is optional

##### 3.3 Else-If

* Statement is evaluated in order

##### 3.4 Switch

The switch statement is a multi-way decision that tests whether an expression matches one of a number of constant integer values, and branches accordingly.  
Example:
```c
switch (expresssion) {
    case const-expr: statements
    case const-expr: statements
    default: statements
}
```

Break and return are the most common ways to leave a switch. 

##### 3.5 Loops - While and For
While:  
```c
while (expression)
    statement
```
For:
```c
for (expr1; expr2; expr3)
    statement
```

##### 3.6 Do-while 

```c
do
    statement
while (expression);
```

##### 3.7 Break and Contiue

* Break statements provide an early exit from *for, while and do*, just as from *switch*.
* A break causes the innermost enclosing loop or switch to be exited immediately. 
* continue statements applies only to loops, and causes the next iteration of the loop to begin. 

##### 3.8 Goto and Labels

* don't

[Table of Contents](README.md)
