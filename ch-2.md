# Chapter 2 - Types, Operators and Expressions

* Variables and constants are the basic data objects manipulated in a program. 
* Declarations list the variables to be used and state their type and initial value. 
* Operators specify what is to be done to the variables. 
* Exprissions combine variables and constants to produce new values. 

##### 2.1 Variable Names

* The underscore: "_" counts as a letter and can be useful to help seperate words, however to not start a variable with one. 
* Upper and lower case are distinct. Example: x and X are different variables. 
* Tradition dictates that you use lower case for variable names and Upper case for symbolic constants. 
* K&R use short names for local variables and long name for external. 

##### 2.2 Data Types and Sizes
There are only a few basic data types in C:
* char - a single byte, capable of holding one character
* int - an integer, typically reflecting the natural size of integers on the host machine
* float - single-precision floating point
* double - double-precision floating point

In addtion, there are a number of qulaifiers that can be applied to these basic types. *short* and *long* apply to integers. 
* short is often 16bits
* long is often 32bits

The qualifier *signed* or *unsigned* may be applied to **char** or any integer. 
* unsigned numbers are always positive or zero

##### 2.3 Constants
* character constant - is an integer, written as one character within single quotes, that holds the value of the numeric value of that character in the machine's character set

Complete set of Escape Characters:
* ```\a``` - alert(bell) character
* ```\b``` - backspace
* ```\f``` - formfeed
* ```\n``` - newline
* ```\r``` - carrage return
* ```\v``` - vertical tab
* ```\\``` - backslash
* ```\?``` - question mark
* ```\'``` - single quote
* ```\"``` - double quote
* ```\ooo``` - octal number
* ```\xhhh``` - hexadecimal number 

A *constant expression* is an expression that involves only constants. Such expressions may be evaluated during compilation rather that run-time, and accordingly maybe used in any place that a constant can occur. 

A *string constant* is a sequence of zero or more characters sruurounde by double quotes. (Also known as a string literal)

String constants can be concatinated at compile time. Ex: 
```"hello," "world"``` is equivolent to ```"hello, world"```

Techincally, a string constant is an array of characters. The internal representation of this has a null character ```'\0'``` at the end.  
The standard library function ```strlen(s)``` returns the length of its caracter string argument, s, excluding the terminal '\0' character

An *enumeration constant* is a list of contstna integer values. 

##### 2.4 Declarations

* All variables must be delcared before use. 
* A declaration specifies a type, and contains a list of one or more variables of that type. 
* External and static variables are initialized to zero by default. 
* Automatic variables, for which there is no explicit initializer, are assignged 'undefined' or 'garbage' values. 
* ```const``` - can be applied to the delaration fo any variable to specify that its value will not be changed. 

##### 2.5 Arithmetic Operators 

The binary arithmetic operators are: +,-,*,/, and the modulus operator %.
* The % operator cannot be applied to a *float* or *double*. 
* PENDAS

##### 2.6 Relational and Logical Operators
The relational operators are: >.>=,<, <=  
These all hav the same presidence, just below them are the equality operatiors: ==, !=  
Relational operators have lower percedence than arithmetic orperators, so the expression ```i < lim-1``` is taken as ```i < (lim-1)```  
Expressions connected with **&&** or **||** are evaluated left to right. 
Evaluation stop as soon as the truth or falsehood is known

##### 2.8 Increment and Decrement Operators

The unusual aspect of these operators is that they may be used as prefix or postfix operators. In both cases the effect is to increment, but the expression ++n increment n *before* its value is used,, while n++ increments *after*. Example: If n is 5, then ```x = n++;``` sets x to 5, but ```x = ++n;``` sets x to 6. 

Increment and ecrement operators can only be applied to variables; an expression like ```(i+j)++``` is illegal.

##### 2.9 Bitwise Operators

* & - bitwise AND
* | - bitwise OR
* ^ - bitwise exclusive OR
* << - left shift
* >> - right shift
* ~ - one's complement (unary)

##### 2.10 Assignment Operators and Expressions




[Table of Contents](README.md)
